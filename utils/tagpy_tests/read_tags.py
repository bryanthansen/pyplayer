#!/usr/bin/python3 -u

import sys
import tagpy
import tagpy.mpeg

if len(sys.argv) < 2:
    sys.stderr.write("ERROR: this script requires at least 1 argument\n")
    sys.exit(2)

fn = sys.argv[1]
f = tagpy.mpeg.File(fn)
tag = f.ID3v2Tag()

for frame_type in tag.frameListMap().keys():
    sys.stdout.write(frame_type + ': ')
    frame_list = tag.frameListMap()[frame_type]
    if frame_list == None: continue
    if len(frame_list) == 0: continue
    sys.stdout.write("frame_list: %s\n" % str(frame_list.toString()))
    found = False
    for frame in frame_list:
        found = True
        sys.stdout.write("  %s\n" % frame.toString())
    if not found: sys.stdout.write("\n")
