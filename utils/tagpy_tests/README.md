# user@host -> eix tagpy
* dev-python/tagpy
     Verfügbare Versionen:   2013.1 {examples PYTHON_TARGETS="python2_7 python3_4 python3_5"}
     Startseite:             http://mathema.tician.de//software/tagpy https://pypi.python.org/pypi/tagpy
     Beschreibung:           Python Bindings for TagLib

