#!/usr/bin/python3 -u

import sys
import tagpy
import tagpy.mpeg
import tagpy.id3v2

if len(sys.argv) < 2:
    sys.stderr.write("ERROR: this script requires at least 1 argument\n")
    sys.exit(2)

fn = sys.argv[1]
fileref = tagpy.FileRef(fn)
file1 = fileref.file()
tag = file1.ID3v2Tag(True)
val = "INT:130"
sys.stderr.write("setting TBPM to %s...\n" % val)
frame = tagpy.id3v2.UniqueFileIdentifierFrame('TBPM', val)
tag.addFrame(frame)
result = file1.save()
if result:
    sys.stderr.write("TBPM set to %s and written to %s\n" % (val, sys.argv[1]))
else:
    sys.stderr.write("ERROR: Failed to write TBPM in %s\n" % sys.argv[1])
