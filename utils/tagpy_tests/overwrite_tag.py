#!/usr/bin/python3 -u

import sys
import tagpy
import tagpy.mpeg

if len(sys.argv) < 2:
    sys.stderr.write("ERROR: this script requires at least 1 argument\n")
    sys.exit(2)

fn = sys.argv[1]
f = tagpy.mpeg.File(fn)
tag = f.ID3v2Tag()

try:
    tag.removeFrame('TBPM', True)
except:
    sys.stderr.write("Exception %s trying to remove the TBPM frame\n"
                     % str(sys.exc_info()))

for frame_type in tag.frameListMap().keys():
    sys.stdout.write(frame_type + ': ')
    frame_list = tag.frameListMap()[frame_type]
    found = False
    for frame in frame_list:
        found = True
        sys.stdout.write("  %s\n" % frame.toString())
    if not found:
        if frame_type == COMM:
            sys.stdout.write("\nCOMM is blank.  Removing.\n")
            tag.removeFrame(frame)
        else:
            sys.stdout.write("\n")


