# PyPlayer Installation

This is a new, experimental Makefile-based installation
It is an upgrade over a shell script for installation
The next step would be integration with a package manager (portage, apt, ipk, pacman)

Make Targets:

    - make install - perform an installation
    - make upgrade - TODO
    - make uninstall - TODO
    - make test - TODO
    - make dist - TODO
    - make clean - TODO
    - make shell-install - show the old shell install script

Installation Locations:
    Makefile Variable  Default Location
    -----------------  -------------------------------
    SOURCE_DIR         /projects/critter_list
    INSTALL_DIR        /var/www/localhost/critter_list
    WSGI_DIR           $(INSTALL_DIR)/wsgi
    CSS_DIR            $(INSTALL_DIR)/css
    DB_DIR             $(INSTALL_DIR)/wsgi/db
    JAVASCRIPT_DIR     $(INSTALL_DIR)/js
    TEMPLATE_DIR       $(INSTALL_DIR)/template
These currently defined in the Makefile; the locations are as follows

