#!/usr/bin/python3 -u
# Bryant Hansen

import sys
import os
import re

pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]

for pid in pids:
    pyplayer_found = False
    try:
        cmdline_file = os.path.join('/proc', pid, 'cmdline')
        cmdline = open(cmdline_file, 'rb').read()
        if len(cmdline) < 2: continue
        sys.stderr.write("\ncmdline: %s\n" % str(cmdline))
        cmdline = cmdline.replace(b'\x00', b'\x20').strip()
        cmdline_str = cmdline.decode("utf-8")
        if re.match(cmdline_str, "pyplayer"):
            sys.stderr.write("pyplayer found in %s\n" % cmdline_str)
            pyplayer_found = True
        sys.stderr.write("cmdline: '%s'\n" % cmdline_str)
    except IOError: # proc has already terminated
        continue
    if pyplayer_found:
        sys.stderr.write("pyplayer_found\n")

