#!/usr/bin/python3 -u
# @author: Bryant Hansen
# @license: GPLv3

"""
A wrapper for mplayer using the slave interfacet
https://mplayerhq.hu/DOCS/tech/slave.txt

This is intended to be used as a library (ie. imported into other python
scripts).  ie.
  import pyplayer
  self.mplif = pyplayer.MPlayerIF()
  mplif.playurl(url)
But may be run independently with an argument.  Example:
  ./pyplayer path-to/my_music_file.mp3

This command launches one thread for the mplayer output monitor and one
thread for mplayer itself.  The current thread continues, exits immediately
and can be used to control and monitor the 2 threads that have been launched.
This allows the main thread to control and monitor the other 2 threads, issuing
play, pause, stop, get_length, get_position, etc. commands and requests.  It
may listen for external commands and requests, interpret them, control the
state of the system and provide feedback.

pyplayerd imports this module and acts as a network-based server, receiving
commands via the network (a json-based interface over TCP/HTTPS)

pyplayer as a library been tested with ipython; and running from the command
line
The main work starts with the playurl() function

It is functional, but requires static state information, which is not
so convenient for the web server application that this designed to interface
with.

A redesign may be necessary in order to connect to an existing instance of
mplayer, without any static information on startup.  This might work like so:
  - mplayer writes stdout and stderr files
  - on startup, look for processes named mplayer
  - check each mplayer process for open files in the /proc/<pid>/ tree
  - verify that the intended files are open
  - seek to the end of stdout(and/or stderr); mplayer normally responds in
    slave mode on stdout
  - send the command to the fifo (or directly to stdin, if possible)
  - get the response from new data from stdout
  - 

This version should become a separate branch before new development

Alternative Option: Daemon Design (local server)
  - implement daemon in background
  - launches, monitors, controls, kills, mplayer subprocess
  - use json for RPC

TODO: consider any queuing mechanisms that should be in place for the
      mplayer interface (either in or out)

"""

import sys
import stat
import os
import time
import signal
import subprocess
import logging
from time import sleep
from datetime import datetime, timedelta
from threading import Thread, Event
from collections import deque
import config
import traceback
from lib.pyplayer_queue import *
from lib.mplayer_constants import *

try:
    from io import StringIO
except ImportError:
    from cStringIO import StringIO

#####################################################
# Defaults

fifo_dir = "/tmp"
fifo_dir = "/projects/critter_list/fifo"

url = ""
ififo = fifo_dir + "/mplayer.stdin.fifo"
log = "/projects/critter_list/log/mplayer.log"

# not currently used
mplayer_opts = [
            "-fs",
            "-ni",
            "-ao", "alsa",
            "-slave",
            "-input",
            "-quiet"
            ]

#LOGFILE = "%s%s%s.log" % (LOGDIR, os.sep, 'player')


#####################################################
# Constants

ME = sys.argv[0]
global logger
logger = None

default_response_wait_time = 1


#####################################################
# Functions

def timestamp():
    return "%s" % datetime.now().strftime("%H:%M:%S.%f")[0:-3]

def TRACE(msg, level = logging.INFO):
    if logger == None or not logger.handlers:
        #m = ("%s: %s.pyplayer(%d): %s"
        #      % (datetime.now().strftime("%H:%M:%S.%f")[0:-3],
        #         ME, os.getpid(), msg))
        #logging.log(level, m)
        sys.stderr.write("pyplayer_mplayer_if.py stderr: %s\n" % (msg))
    else:
        # @NOTE: this seems to create a second logger automatically; the
        #        function that owns us ends up double-logging
        #logging.log(level, msg)
        logger.log(level, msg)


#####################################################
# MPlayerIF: the main class of this file

class MPlayerIF(Play_Queue):

    TRACE("MPlayerIF pre-init")
    mplthread = None
    #state = States.INVALID
    mplproc = None
    read_thread = None
    fifo = ififo
    mpl_fifo_fd = None
    mplayer_out_ring_buffer = None

    global logger

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    # create console handler and set level to debug
    ch = logging.StreamHandler()
    #ch.setLevel(logging.DEBUG)
    # create formatter
    formatter = logging.Formatter(
                "%(asctime)s: pyplayerd: %(name)s: %(levelname)s: %(message)s")
    # add formatter to ch
    ch.setFormatter(formatter)
    if logger.handlers:
        sys.stderr.write("mplayer.py %s already has log handlers: %s\n"
                         % (sys.argv[0], str(logger.handlers)))
    else:
        # add ch to logger
        logger.addHandler(ch)
        logger.debug("MPlayerIF logger initialized.")

    def __init__(self):
        # TODO: determine how to share this ring buffer between
        logger.debug("MPlayerIF.__init__")
        self.state = States.INVALID
        str_level = "INFO"
        str_level = "DEBUG"
        init_msg = (
            "%s: %s.MPlayerIF.__init__: initializing logfile %s at %s level\n"
            % (timestamp(), ME, config.LOGFILE, str_level))
        logger.info(init_msg)
        sys.stderr.write(init_msg)
        self.url = ""
        """
        try:
            level = getattr(logging, str_level)
            format = '%(asctime)-15s: pyplayer_mplayer_if.py %(levelname)s %(message)s'
            logging.basicConfig(filename=config.LOGFILE, format=format, level=level)
        except:
            sys.stderr.write("exception initializing logfile %s:\n%s\n"
                             % (ME, str(str(sys.exc_info())))
                            )
        """ 
        #logger.debug("MPlayerIF.__init__: creating ring buffer for mplayer output")
        self.mplayer_out_ring_buffer = deque(maxlen=100)
        self.track_complete_callback = None

    def set_track_complete_callback(self, function):
        self.track_complete_callback = function

    def flush_output(self, stdouterr):
        # TODO: implement me!
        pass

    def __set_stdout_nonblocking(self, p):
        import fcntl
        filemode = fcntl.fcntl(p.stdout.fileno(), fcntl.F_GETFL)
        logger.info("filemode = %s" % hex(filemode))
        filemode |= os.O_NONBLOCK
        logger.info("filemode = %s" % hex(filemode))
        ret = fcntl.fcntl(p.stdout.fileno(), fcntl.F_SETFL, filemode)
        logger.info("filemode set.  ret = " + str(ret))

    def __set_stdout_blocking(self, p):
        import fcntl
        filemode = fcntl.fcntl(p.stdout.fileno(), fcntl.F_GETFL)
        logger.info("filemode = %s" % hex(filemode))
        # try the ^= operator to toggle the specific bit
        # TODO: verify that this works
        filemode ^= os.O_NONBLOCK
        logger.info("filemode = %s" % hex(filemode))
        ret = fcntl.fcntl(p.stdout.fileno(), fcntl.F_SETFL, filemode)
        logger.info("filemode set.  ret = " + str(ret))

    def __subprocess_output_readline(self, stdouterr, expect=""):
        """
        This function dumps all pending data from the mplayer stdout or stderr
        buffers and will optionally match the "expect" string.
        Note: file blocking mode has an effect on this function

        @param stdout - the subprocess.stdout (or .stderr) member when created
        with stdout=subprocess.PIPE and/or stderr=subprocess.PIPE options
        @param expect - the name of the variable that we're looking for in the
        output.  this is the expectation that the output will contain a
            var=...
        example:
            write:
            ANS_volume=87.909088
        """
        retstr = ""
        try:
            # TODO: determine how to make this blocking
            output = stdouterr.readline().decode("utf-8")
        except:
            logger.error(
                "Exception in __subprocess_output_readline: %s"
                % str(sys.exc_info())
            )
            logger.error(
                "Exception str(stdouterr) = %s"
                % str(stdouterr)
            )
            return "Exception"
        output = output.rstrip()
        if output == 'ANS_ERROR=PROPERTY_UNAVAILABLE':
            return "PROPERTY_UNAVAILABLE"
        if len(output) < 1:
            return ""
        #logger.debug("__subprocess_output_readline: output = %s" % output)
        if len(expect) > 0:
            split_output = output.split(expect + '=', 1)
            # we have found it
            # FIXME: does the split_output[0] param need a whitespace trim?
            if len(split_output) == 2 and split_output[0] == '':
                if len(retstr) > 0:
                    logger.info(
                        "multiple values exist matching '%s'.  "
                        "changing result from '%s' to '%s'"
                        % (expect, retstr, split_output[1])
                    )
                retstr = split_output[1]
        else:
            retstr = output
        return retstr

    def __mplayer_output_loop(self, expect="", response_wait_time=3):
        """
        This function dumps all pending data from the mplayer stdout buffer and
        will optionally match the "expect" string.
        Note: file blocking mode has an effect on this function

        mplproc.stdout - the subprocess.stdout (or .stderr) member when created
        with stdout=subprocess.PIPE and/or stderr=subprocess.PIPE options

        @param expect - the name of the variable that we're looking for in the
        output.  this is the expectation that the output will contain a
            var=...
        example:
            write:
            ANS_volume=87.909088
        """
        __FUNCTION__ = sys._getframe().f_code.co_name
        logger.debug("enter %s: expect = '%s'" % (__FUNCTION__, str(expect)))

        retstr = ""
        output = ""

        # TODO: loop until time elapsed
        start_time = None
        if response_wait_time > 0:
            start_time = time.time()
        while start_time == None or time.time() - start_time < response_wait_time:
            if start_time == None:
                start_time = time.time()
            #logger.debug("__mplayer_output_loop: started at %s.  Elapsed time = %s"
            #      % (str(start_time), str(time.time() - start_time)))
            try:
                # TODO: determine how to make this blocking
                output = mplproc.stdout.readline().decode("utf-8")
                logger.info("%s: mplayer stdout: %s" % (__FUNCTION__, output))
            except:
                output = ""
            if len(output) < 1:
                try:
                    if len(self.mplayer_out_ring_buffer) > 0:
                        output = self.mplayer_out_ring_buffer.pop()
                        logger.info(
                            "%s: (line %d): ring buffer output: '%s'"
                            % (__FUNCTION__,
                               len(self.mplayer_out_ring_buffer),
                               str(output))
                        )
                        #logger.debug("ring buffer size = %d"
                        #      % (len(self.mplayer_out_ring_buffer)))
                except:
                    break
            # TODO: consider any buffer santizing that might be necessary
            #logger.debug("__mplayer_output_loop: output = '%s'.  expect = %s"
            #      % (output, expect))
            logger.info("%s: un-stripped output: '%s'" % (__FUNCTION__, output))
            output = output.rstrip()
            logger.info("%s: stripped output: '%s'" % (__FUNCTION__, output))
            if len(output) < 1:
                time.sleep(0.5)
                continue
            if output == 'ANS_ERROR=PROPERTY_UNAVAILABLE':
                continue
            if len(expect) > 0:
                split_output = output.split('=')
                logger.info("%s: split output = '%s', len(split_output) = %d"
                      % (__FUNCTION__, str(split_output), len(split_output)))
                if split_output[0] == expect and len(split_output) == 2:
                    logger.info("%s: received expected output %s = %s"
                          % (__FUNCTION__, expect, str(split_output[1])))
                    retstr = split_output[1]
                    break
                else:
                    logger.info("buffer %s does not match expect string %s"
                          % (str(split_output), str(expect)))
            else:
                if len(retstr) > 0:
                    retstr += '\n'
                retstr += output
                #break
        return retstr

    def dump_output(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        output = ""
        try:
            if len(self.mplayer_out_ring_buffer) <= 0:
                return "<no output>"
            output = self.mplayer_out_ring_buffer.popleft()
            while len(self.mplayer_out_ring_buffer) > 0:
                output += "\n%s" % self.mplayer_out_ring_buffer.popleft()
        except:
            logger.error("%s: exception reading mplayer output ring buffer"
                  % __FUNCTION__)
            e = sys.exc_info()
            logger.error("exception info: %s" % str(e))
            traceback.print_exc()
            return "<exception reading mplayer output ring buffer>"
        return output

    def get_state(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        #if self.mplproc == None:
        #    self.state = MPLAYER_NOT_RUNNING
        #else:
        #    state = self.mplproc.poll()
        logger.info("%s: self.state = '%s', type %s"
                  % (__FUNCTION__, str(self.state), str(type(self.state))))
        logger.info("%s: states: '%s'"
                  % (__FUNCTION__, str(States)))
        States
        return self.state

    def status(self):
        """
        @brief This function calls pyplayerd status
        """
        __FUNCTION__ = sys._getframe().f_code.co_name
        result = "failed to get result from pyplayerd.py status"
        command = ["pyplayerd.py", "status"]
        try:
            proc = subprocess.Popen(
                                command,
                                shell=False,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE
                              )
            proc.wait()
            proc_comm = proc.communicate()
            logger.info("%s process stdout: %s (type = %s)"
                  % (command, str(proc_comm[0]), str(type(proc_comm[0]))))
            logger.info("%s process stderr: %s (type = %s)"
                  % (command, str(proc_comm[1]), str(type(proc_comm[1]))))
            result = str(proc_comm[0].decode('utf-8'))
            if len(result) < 1:
                result = str(proc_comm[1].decode('utf-8'))
        except:
            logger.info("exception %s calling subprocess.Popen(%s)"
                  % (str(sys.exc_info()), str(command)))

        sys.stdout.write("%s\n" % result)
        return result

    def perform_command(self,
                        cmd,
                        expect="",
                        response_wait_time=default_response_wait_time):
        __FUNCTION__ = sys._getframe().f_code.co_name
        logger.info("%s: sending cmd '%s' (expecting '%s')"
              % (__FUNCTION__, cmd, str(expect)))

        if self.mpl_fifo_fd == None:
            logger.warning("%s: warning: no mplayer fifo to send commands to"
                  % __FUNCTION__)
            self.state = States.MPLAYER_NOT_RUNNING
            logger.warning("mplayer not running - could not send command.  "
                  "set state to MPLAYER_NOT_RUNNING")
            return "unavailable"

        if len(expect) <= 0:
            logger.info("checking for expected response string for this command (%s)"
                  % str(cmd))
            try:
                expect = Responses[cmd]
                #logger.debug("Responses[%s] returned '%s'" % (cmd, str(expect)))
                if expect == None: expect = ""
            except:
                logger.error("exception calling Responses[%s]" % (cmd))
                expect = ""
            logger.info("expected response string to command '%s' is '%s'"
                  % (str(cmd), str(expect)))

        res = ""
        try:
            logger.info("%s: clearing mplayer output ring buffer; the next buffer "
                  "entry should be the response to this request"
                  % __FUNCTION__)
            self.mplayer_out_ring_buffer.clear()
            res = os.write(self.mpl_fifo_fd,
                           (cmd + '\n').encode(encoding='UTF-8'))
        except BrokenPipeError:
            logger.error("perform_command: BrokenPipeError: is mplayer running?")
            return "MPLAYER_NOT_RUNNING"
        except e:
            logger.error("perform_command: exception calling os.write(): %s, errno=%d"
                  % (str(e), e.errno))
            traceback.print_exc()
            return (
              "exception in perform_command calling os.write(cmd): %s (%d)"
              % (str(e), e.errno))
        logger.info(
            "%s: %s (type(result) = %s, val(result = %s))"
            % (__FUNCTION__, cmd, str(type(res)), str(res))
        )
        sleep(0.05)
        output = self.__mplayer_output_loop(
                            expect=expect,
                            response_wait_time=response_wait_time)
        if len(output) > 0:
            logger.info(
                "%s: mplayer output returned '%s'"
                % (__FUNCTION__, str(output))
            )
        else:
            logger.info(
                "%s: __mplayer_output_loop returned a zero-length "
                "string"
                % __FUNCTION__
            )
        return output

    # @TODO get rid of this big block of duplicate code with a better technique
    def get_time_pos(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_current_time(self):
        return self.get_time_pos()

    def get_percent_pos(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_track_length(self):
        return self.get_time_length()

    def get_audio_samples(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_file_name(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_meta_comment(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_meta_genre(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_meta_year(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_sub_visibility(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_time_length(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_vo_fullscreen(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_video_bitrate(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_video_codec(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_video_resolution(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_audio_bitrate(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_meta_title(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_meta_artist(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_meta_album(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    def get_meta_track(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        try:
            return self.perform_command(__FUNCTION__,
                                        expect=Responses[__FUNCTION__])
        except:
            return str(sys.exc_info())

    # some aliases
    def get_title(self):        return self.get_meta_title()
    def get_url(self):          return self.url
    def get_artist(self):       return self.get_meta_artist()
    def get_album(self):        return self.get_meta_album()
    def get_track(self):        return self.get_meta_track()

    def get_volume1(self):
        return self.perform_command('volume')

    def get_volume2(self):
        return self.perform_command('get_property volume')

    def set_current_pos(self, position):
        return self.perform_command('seek %d 2' % position)

    def seek(self, reltime):
        return self.perform_command('seek %d 0' % reltime)

    def seek_start(self):
        return self.perform_command('seek 0 2')

    def seek_end(self):
        return self.perform_command('seek 100 1')

    '''
    def gong(self):
        logger.info("attempting to play %s from current directory '%s'"
              % os.getcwd())
        self.playurl(config.gongsound)
        result = self.perform_command('seek 100 1')
        # TODO: do we need to sleep here to avoid the next track running into us?
        return result
    '''

    def gong(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        #if self.mplproc == None:
        #    self.state = MPLAYER_NOT_RUNNING
        #else:
        #    state = self.mplproc.poll()
        logger.info("%s: self.state = '%s', type %s"
                  % (__FUNCTION__, str(self.state), str(type(self.state))))
        retval = self.stop()
        if os.path.exists(config.gongsound):
            # self.launch_player(config.gongsound)
            self.play_synchronous(config.gongsound)
        else:
            logger.error("gong ERROR: gong sound '%s' not found" % config.gongsound)
        retval = self.play_next_url()
        return retval

    def stop(self):
        __FUNCTION__ = sys._getframe().f_code.co_name
        self.state = States.STOPPED
        logger.info("%s: self.perform_command('stop').  state set to STOPPED"
              % __FUNCTION__)
        retval = self.perform_command('stop')
        logger.info("%s response: %s (type = %s)"
              % (__FUNCTION__, str(retval), str(type(retval)))
            )
        return retval

    def quit(self):
        logger.info("self.perform_command('quit')")
        r = self.perform_command('quit')
        logger.info("self.perform_command('quit') done.  response = %s" % str(r))
        return r

    def pause(self):
        return self.perform_command('pause')

    def do_command(self, command):
        return self.perform_command(command)

    def get_all_information(self):
        self.all_info = StringIO()
        __FUNCTION__ = str(sys._getframe().f_code.co_name)
        for s in Responses.keys():
            logger.info("%s: performing command '%s'..." % (__FUNCTION__, s))
            a = ""
            try:
                a = self.perform_command(s)
                self.all_info.write("%s: %s" % (s, str(a)))
            except:
                logger.info("%s: exception getting parameter %s: %s"
                      % (__FUNCTION__, str(s), str(sys.exc_info())))
                self.all_info.write("Exception %s reading %s"
                                    % (str(sys.exc_info), s))
            logger.info("%s: parameter %s = '%s'" % (__FUNCTION__, s, str(a)))
        for s in properties.keys():
            logger.info("%s: writing 'get_property %s' to mpl_fifo_fd..."
                  % (__FUNCTION__, s))
            a = ""
            try:
                os.write(
                    self.mpl_fifo_fd,
                    ("get_property %s\n" % s).encode(encoding='UTF-8')
                )
                a = self.perform_command(
                        "get_property %s" % s
                    )
                self.all_info.write("property %s: %s" % (s, str(a)))
            except:
                logger.error("%s: exception getting property %s: %s"
                      % (__FUNCTION__, str(s), str(sys.exc_info())))
                self.all_info.write("Exception %s reading property %s"
                                    % (str(sys.exc_info), s))
            logger.info(
                "%s: property %s = '%s'"
                % (__FUNCTION__, s, str(a))
            )
        logger.info("%s: all strings and properties queried." % __FUNCTION__)
        return self.all_info.getvalue()

    def mplayer_thread(self, threadName, mplayer_start_event, url):
        """
        This is a thread which is a wrapper for the process
        The thread further isolates the mplayer process from the main
        python process.
        One goal is so that signals passed to the python process are not
        automatically propogated to the mplayer process; this can be seen
        running ipython and pressing ctrl-C after which mplayer is running.
        Hopefully the thread insulates this.

        TODO: consider tempfile/directory handling like this:
        http://stackoverflow.com/questions/1430446/create-a-temporary-fifo-named-pipe-in-python
        """
        __FUNCTION__ = sys._getframe().f_code.co_name
        event_set = False
        logger.debug("enter %s; launching %s" % (__FUNCTION__, url))
        command = [
                    "mplayer",
                    "-vo", "null",
                    "-ao", "alsa",
                    "-slave",
                    "-input",
                    "file=" + self.fifo,
                    "-quiet",
                    url
                  ]
        '''
        command = [
                    "mplayer",
                    "-fs",
                    "-ni",
                    "-ao", "alsa",
                    "-slave",
                    "-input",
                    "-quiet",
                    "file=" + self.fifo,
                    url
                  ]
        '''
        try:
            # TODO: loop here with a queue of tracks
            self.mplayer_out_ring_buffer.clear()
            self.mplproc = subprocess.Popen(
                                command,
                                shell=False,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE
                           )
            self.url = url
            #self.__set_stdout_blocking(self.mplproc)
            #self.state = States.PLAYING
            # TODO: poll() ASAP to verify running state
            self.mpl_fifo_fd = os.open(self.fifo, os.O_WRONLY)
            logger.info(
                "launched mplayer process.  "
                "command = %s  "
                "process id = %s  "
                "process id file = %s  "
                "process poll() / exit code = %s"
                % (str(command),
                   str(self.mplproc.pid),
                   config.MPLAYER_PIDFILE,
                   str(self.mplproc.poll())
                )
            )
            # Write the PID file
            with open(config.MPLAYER_PIDFILE,'w') as f:
                print(self.mplproc.pid,file=f)
            mplayer_start_event.set()
            event_set = True
            self.mplproc.wait()
            if self.state == States.PLAYING:
                self.state = States.TRACK_COMPLETE
                logger.info("%s: state set to TRACK_COMPLETE" % __FUNCTION__)
                if self.track_complete_callback != None:
                    try:
                        self.track_complete_callback()
                    except:
                        logger.error(
                            "%s: exception '%s' calling track_complete_callback"
                            % (__FUNCTION__, str(sys.exc_info())))
                        traceback.print_exc()
            self.url = ""
            logger.info("%s: self.mplproc.wait() completed" % __FUNCTION__)
        except:
            logger.error("player(%s) ERROR: failed to create read thread" % url)
            e = sys.exc_info()
            logger.error("exception info: %s" % str(e))
            traceback.print_exc()
            if not event_set:
                mplayer_start_event.set()

    def play_synchronous(self, url):
        __FUNCTION__ = sys._getframe().f_code.co_name
        logger.debug("enter %s; launching %s" % (__FUNCTION__, url))
        command = [
                    "mplayer",
                    "-vo", "null",
                    "-ao", "alsa",
                    "-slave",
                    "-input",
                    "file=" + self.fifo,
                    "-quiet",
                    url
                  ]
        try:
            local_mplproc = subprocess.Popen(
                                command,
                                shell=False,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE
                           )
            logger.info("launched mplayer process.  command = '%s'" % str(command))
            # Write the PID file - consider doing this to prevent too many parallel threads running this
            #with open(config.GONG_PIDFILE,'w') as f:
            #    print(local_mplproc.pid,file=f)
            local_mplproc.wait()
            logger.info("%s: local_mplproc.wait() completed" % __FUNCTION__)
        except:
            logger.error("player(%s) ERROR: failed to create read thread" % url)
            e = sys.exc_info()
            logger.error("exception info: %s" % str(e))
            traceback.print_exc()


    def launch_player(self, url):
        __FUNCTION__ = sys._getframe().f_code.co_name
        if not self.__verifyFifoPath(fifo_dir):
            logger.info(
                "%s(%s): failed to verify the path to the fifos (%s)"
                % (__FUNCTION__, url, fifo_dir)
            )
            return False

        if not self.__verifyFifo(self.fifo):
            logger.error(
                "%s(%s): failed to __verifyFifos.  Exiting abnormally"
                % (__FUNCTION__, url)
            )
            return False
        mplayer_start_event = Event()
        # start the mplayer thread, which will contain the mplayer process
        self.mplthread = Thread(
                    target = self.mplayer_thread,
                    args = ("mplayerThread", mplayer_start_event, url)
                )
        self.mplthread.start()
        mplayer_start_event.wait()

        #self.__set_stdout_nonblocking(self.mplproc)
        self.state = States.PLAYING
        logger.info("%s: mplayer thread started.  set state to PLAYING"
              % __FUNCTION__)
        if self.mplproc == None:
            logger.warning("%s ERROR: self.mplproc not set." % __FUNCTION__)
            return False
        else:
            if self.mplproc.poll() == None:
                return True
            else:
                return False

    def __verifyFifoPath(self, fifo_dir):
        __FUNCTION__ = sys._getframe().f_code.co_name
        if os.path.exists(fifo_dir):
            if os.path.isdir(fifo_dir):
                logger.debug("%s: %s exists" % (__FUNCTION__, fifo_dir))
                return True
            else:
                logger.warning(
                    "%s: %s exists, but not as a directory.  "
                    "Cannot continue."
                    % (__FUNCTION__, fifo_dir)
                )
                return False
        else:
            logger.info("%s: %s does not exist.  Creating..."
                  % (__FUNCTION__, fifo_dir))
            try:
                os.mkdir(fifo_dir)
            except:
                logger.error(
                    "%s: exception creating fifo directory %s: %s"
                    % (__FUNCTION__, fifo_dir, str(sys.exc_info()))
                )
            if os.path.isdir(fifo_dir):
                return True
            else:
                logger.error(
                    ": failed to create fifo directory %s"
                    % (__FUNCTION__, fifo_dir)
                )
                return False

    def __verifyFifo(self, fifo):
        __FUNCTION__ = sys._getframe().f_code.co_name
        if os.path.exists(fifo):
            # if it exists, but it not a fifo, then also show an error
            if not stat.S_ISFIFO(os.stat(ififo).st_mode):
                logger.error(
                    "%s: ERROR: file %s is not a fifo!  "
                    " Exiting abnormally"
                    % (__FUNCTION__, ififo)
                )
                return False
            else:
                logger.info(
                    "%s: fifo %s already exists in the filesystem and "
                    "is indeed a fifo.  We must delete the old one."
                    % (__FUNCTION__, fifo)
                )
                try:
                    os.remove(fifo)
                    if os.path.exists(fifo):
                        logger.error(
                            "%s ERROR: failed to remove old fifo %s"
                            % (__FUNCTION__, fifo)
                        )
                        return False
                except:
                    logger.error("Exception in %s: %s"
                          % (__FUNCTION__, str(sys.exc_info())))
                    return False
        try:
            logger.info(
                "%s: creating fifo %s..."
                % (__FUNCTION__, fifo)
            )
            # TODO: figure out how to set mode here; never seemed to work
            #       0644, O644 and '0644' were all tried, with no success
            #os.mkfifo(fifo, mode=O644)
            os.mkfifo(fifo)
            if not os.path.exists(fifo):
                logger.error("ERROR: %s: Failed to create fifo %s"
                      % (__FUNCTION__, fifo))
                return False
        except:
            logger.error(
                "%s ERROR: failed to create fifo %s.  "
                "Description: %s  %s"
                "Exiting abnormally"
                % (__FUNCTION__, fifo, sys.exc_info()[0], str(sys.exc_info()))
            )
            return False
        return True

    def __subproc_output_monitor(self, threadName, stdouterr):
        __FUNCTION__ = sys._getframe().f_code.co_name
        empty_count = 0
        logger.info(
            "start read loop.  change to blocking reads for efficiency.  "
            "str(stdouterr) = %s" % str(stdouterr)
        )
        while True:
            try:
                out = self.__subprocess_output_readline(stdouterr)
                logger.info(
                    "%s: mplayer output: %s"
                    % (__FUNCTION__, str(out))
                )
                if len(out) > 0:
                    # NOTE: the append and pop functions of the ring buffer
                    #       (a deque object) are thread-safe
                    self.mplayer_out_ring_buffer.append(out)
                    empty_count = 0
                else:
                    procstat = self.mplproc.poll()
                    empty_count += 1
                    if procstat == None:
                        logger.info(
                            "%s: "
                            "Empty output #%d received from "
                            "subprocess.readline().  "
                            "subprocess.poll() = %s.  type(procstat) = %s  "
                            "Continuing read loop"
                            % (__FUNCTION__,
                               empty_count,
                               str(procstat),
                               str(type(procstat))
                            )
                        )
                    else:
                        # Here is where to check if there's another URL in the
                        # queue
                        # If we're stopped, then we should not get the next in
                        # the queue; just stop playing
                        # if  self.state != States.PLAYING
                        # and self.state != States.TRACK_COMPLETE and :
                        if self.state == States.STOPPED:
                            logger.info("Terminating mplayer read loop; state not "
                                  "set as playing.  state = %s"
                                  % str(self.state))
                            return 1
                        logger.info("%s: state = %s; getting next in queue..."
                              % (__FUNCTION__, str(self.state)))
                        if self.play_next_url():
                            return "playing next in queue"
                        else:
                            return 1
                if empty_count >= 3:
                    logger.info(
                        "%s: 3 empties in a row.  "
                        "bailing out." % __FUNCTION__
                    )
                    return 1
                # TODO: this should update a dictionary of vars (with timestamps)
            except:
                logger.error(
                    "%s: Exception in read loop: %s.  "
                    "Terminating."
                    % (__FUNCTION__, str(sys.exc_info()))
                )
                traceback.print_exc()
                break
            sleep(0.1)

        return 0

    def playurl(self, url):
        __FUNCTION__ = sys._getframe().f_code.co_name
        logger.debug("%s: url = '%s'" % (__FUNCTION__, str(url)))
        # TODO: update last_played here!
        self.launch_player(url)
        """
        out = self.__mplayer_output_loop(self.mplproc.stdout)
        logger.info("%s: cmd = '%s', out = '%s'" % (__FUNCTION__, cmd, str(out)))

        """
        # start the read thread
        try:
            self.read_thread = Thread(
                        target = self.__subproc_output_monitor,
                        args = ("mplayerStdoutMonitor", self.mplproc.stdout, )
                    )
            # wait for the read thread to complete
            self.read_thread.start()
            logger.info(
                "launched mplayerStdoutMonitor.  str(read_thread) = %s  "
                "self.mplproc.stdout = %s"
                % (str(self.read_thread), str(self.mplproc.stdout))
            )
        except:
            logger.error("player(%s) ERROR: failed to create read thread" % url)
            e = sys.exc_info()
            logger.error("exception info: %s" % str(e))
            traceback.print_exc()
            return False
        return True

    def play(self):
        # here is where to check if there's another URL in the queue
        __FUNCTION__ = sys._getframe().f_code.co_name
        next_url = self._get_next_in_queue()
        if next_url == None or len(next_url) < 1:
            logger.info("%s: no urls are in the queue" % __FUNCTION__)
        else:
            logger.info("%s: launching next url %s" % (__FUNCTION__, next_url))
            self.playurl(next_url)
        return "playing next in queue"

    def play_next_url(self):
        # here is where to check if there's another URL in the queue
        __FUNCTION__ = sys._getframe().f_code.co_name
        next_url = self._get_next_in_queue()
        if next_url == None or len(next_url) < 1:
            logger.info("%s: no urls are in the queue" % __FUNCTION__)
            return False
        else:
            logger.info("%s: launching next url %s" % (__FUNCTION__, next_url))
            return self.playurl(next_url)

    def help(self):
        help_str = (
            "%s(%d) - Help heading on stdout\n"
             % (ME, os.getpid())
        )
        sys.stdout.write("%s - on stdout\n" % (help_str))
        sys.stderr.write("%s - on stderr\n" % (help_str))
        #sys.stdout.write("%s(%d) - Help heading on stdout\n" % (ME, os.getpid()))
        #sys.stderr.write("%s(%d) - Help heading on stderr\n" % (ME, os.getpid()))
        return (
            "%s.lib.pyplayer_mplayer_if.py(%d) help() information: "
            "TODO: send useful help information; determine best size"
            % (ME, os.getpid())
        )

    def cleanup(self):
        try:
            os.remove(self.fifo)
        except:
            logger.error(
                "Exception cleaning up on return: %s.  Aborting."
                % str(sys.exc_info())
            )
            return 5
        return 0


#####################################################
# Main
# Run in the case of testing
# ./pyplayer.py my_music_file.mp3

if __name__ == "__main__":
    sys.stderr.write("launched %s with args %s\n" % (sys.argv[0],
                                                     str(sys.argv[1:])))
    if len(sys.argv) <= 1 or sys.argv[1] == "help":
        sys.stderr.write(
            "When called from the command line, this function can take 1 "
            "argument, a URL to play (such as the path to a file on the local "
            "filesystem)\n")
        sys.stderr.write("  %s url\n")
        sys.exit(1)
    else:
        mplif = MPlayerIF()
        mplif.add_url_to_queue(sys.argv[1])
        sys.stderr.write("mplif.read_thread = %s\n" % str(mplif.read_thread))
        mplif.read_thread.join()
        sys.stderr.write("completed %s(%s)\n" % (sys.argv[0], sys.argv[1]))
        mplif.cleanup()
        sys.exit(0)
