Fallback to home dir for logfiles and PID file if dir does not exist and cannot be created with current permissions
  - error must go to stdout or stderr if occurs!  it was only found in the log file

Add support for remote pyplayerc instances
  - all commands will need a host argument
    - as option?

ADD YOUTUBE URLS!

ADD VOLUME COMMANDS AND MIXER SETTINGS

rewrite pyplayer.py main to be a better command line interface
  - help
  - play urls
  - add urls to queue
  - get all status  

String to function, so "./pyplayer.py command args..." will execute the function "command" within pyplayer.py
Make help menu for pyplayer.py args

On startup, dump supported functions to command line

Auto-generate test files
  - create .wav files with sin waves and compressed with various codecs & settings
  - consider video - what are standard auto-gen video test signals?
    - gstreamer video test pattern?

Consider how a gstreamer implementation would look like?
  - separate version as mplayer or combined?

What about mpd?


Fix character set issues in filesystem:
=====================================================================================================================
/projects/critter_list/pyplayer 
gink@bella-gentoo -> ./test/012_queue_multiple_urls.py /data/media/mp3/Feindflug/Volk\ und\ Armee/*
sys.argv len = 16
sys.argv = /data/media/mp3/Feindflug/Volk und Armee/01 - Feindflug - Einmarsch.mp3
sys.argv = /data/media/mp3/Feindflug/Volk und Armee/02 - Feindflug - Standgericht.mp3
sys.argv = /data/media/mp3/Feindflug/Volk und Armee/03 - Feindflug - AK 47.mp3
sys.argv = /data/media/mp3/Feindflug/Volk und Armee/04 - Feindflug - Truppenschau.mp3
sys.argv = /data/media/mp3/Feindflug/Volk und Armee/05 - Feindflug - Feuerpause.mp3
sys.argv = /data/media/mp3/Feindflug/Volk und Armee/06 - Feindflug - Schmerzgrenze.mp3
sys.argv = /data/media/mp3/Feindflug/Volk und Armee/07 - Feindflug - Tauchfahrt.mp3
sys.argv = /data/media/mp3/Feindflug/Volk und Armee/08 - Feindflug - Feindbild.mp3
sys.argv = /data/media/mp3/Feindflug/Volk und Armee/09 - Feindflug - Sperrfeuer.mp3
exception adding urls to the queue
Traceback (most recent call last):
  File "./test/012_queue_multiple_urls.py", line 56, in <module>
    print("sys.argv = %s" % sys.argv[n])
UnicodeEncodeError: 'utf-8' codec can't encode character '\udce4' in position 77: surrogates not allowed
=====================================================================================================================


