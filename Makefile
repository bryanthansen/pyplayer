# Bryant Hansen
# PyPlayer Makefile
# 20160130
#
# Used for installing and uninstalling the pyplayer client & server

# @TODO: must move much of this from the pyplayer makefile to the critter makefile

# Root source directory

MAKE = make

SOURCE_DIR = /projects/critter_list/pyplayer2

PYPLAYERD = /usr/local/bin/pyplayerd.py
RUN_DIR = /run/pyplayer
RUN_FILE = $(RUN_DIR)/pyplayerd.pid
LOG_FILE = /var/log/pyplayer/pyplayerd.log
PYPLAYERD_USER = pyplayerd
PYPLAYERD_GROUP = pyplayerd
#PROJECT_DIR = /opt/critter_list

CONF_DIR = /etc/pyplayer
CONF_FILE = pyplayerd.conf
CONF_FILE_GLOBAL = $(CONF_DIR)/$(CONF_FILE)
CONF_FILE_USER = ${HOME}/pyplayer/$(CONF_FILE)
CONF_FILE_LOCAL = ./$(CONF_FILE)

LIB_SRC_DIR = $(SOURCE_DIR)/lib
INSTALL_DIR = /opt/pyplayer/
LIB_INSTALL_DIR = $(INSTALL_DIR)/lib
LIBRARY_FILES = \
	__init__.py \
	mplayer_constants.py \
	pyplayer_lib.py \
	pyplayer_mplayer_if.py \
	pyplayer_queue.py \
	pyplayerc.py \
	python_process_list.py \
	trace.py
LIB_INSTALL_FILES = $(addprefix $(LIB_INSTALL_DIR)/, $(LIBRARY_FILES))

USER_RUN_DIR=$(HOME)/.run/pyplayerd/
USER_BASHCOMP_DIR=$(HOME)/.bash_completion.d/
USER_LOGDIR=$(HOME)/.log/pyplayerd/

INSTALL_METHOD_COPY = echo cp
INSTALL_METHOD_COPY_A = echo cp -a
INSTALL_METHOD_LINK = echo ln -fs
INSTALL_METHOD = $(INSTALL_METHOD_COPY)

.PHONY: default all install test clean client server install_user install_root

default:
	cat README.md

test:
	echo TODO

client:
	echo TODO

server:
	echo TODO

$(USER_RUN_DIR) \
$(USER_BASHCOMP_DIR) \
$(USER_LOGDIR) \
$(CONF_DIR) \
$(LIB_INSTALL_DIR) \
:
	@[ -d $@ ] || echo mkdir -p $@

$(HOME)/.bash_completion.d/pyplayer:
	# TODO: this should be linking to the location of the rest of the bashcomp files in /usr/share/bash_completion
	echo $(INSTALL_METHOD_LINK) /usr/share/bash-completion/pyplayer $@

$(CONF_FILE_GLOBAL): $(CONF_DIR)
	[ -f $@ ] || $(INSTALL_METHOD_COPY) $(SOURCE_DIR)/$(CONF_FILE) $@

$(CONF_FILE_USER):

#$(LIB_INSTALL_DIR)/%.py: $(SOURCE_DIR)/lib/%.py  $(LIB_INSTALL_DIR)
#	[ -f $@ ] || echo cp -a $< $@

$(LIB_INSTALL_DIR)/%.py: $(LIB_SRC_DIR)/%.py $(LIB_INSTALL_DIR)
	@# development mode: make symlinks, so changes in the source dir are effective immediately
	@#ln -s $< $@
	@# user mode: copy the files, as well as set rights & perms
	@$(INSTALL_METHOD_COPY) $< $@

install_user: $(USER_RUN_DIR) $(USER_BASHCOMP_DIR) $(USER_LOGDIR) $(CONF_DIR) $(LIB_INSTALL_DIR)
	@echo "TODO install_user.  USER_RUN_DIR=${USER_RUN_DIR}; USER_RUN_DIR=$(USER_RUN_DIR)" >&2
	@[ ! -d $(USER_RUN_DIR) ] || [ $(shell stat --format=%a $(USER_RUN_DIR) 2>/dev/null) == 770 ] || echo chmod 770 $(USER_RUN_DIR)

install_root: $(CONF_FILE_GLOBAL) $(CONF_DIR) $(LIB_INSTALL_DIR)
	@echo -e "\n##########################"
	@echo -e "# Performing a series of tests to verify the installation and perform steps if necessary #"
	@echo -e "# Errors may be generated due to insufficient privilege; an attempt will be made to provide easy instructions to complete the installation at the appropriate privilege level #"
	@echo -e "##########################\n"
	@grep pyplayerd /etc/group > /dev/null || echo groupadd pyplayerd
	@[ -d /run/pyplayer ] || echo mkdir -p /run/pyplayer
	@[ ! -d /run/pyplayer ] || [ $(shell stat --format=%G /run/pyplayer) == pyplayerd ] || echo chgrp pyplayerd /run/pyplayer
	@[ ! -d /run/pyplayer ] || [ $(shell stat --format=%a /run/pyplayer) == 770 ] || echo chmod 770 /run/pyplayer
	@# echo chmod g+wrx /run/pyplayer

	@[ ! -d /usr/share/bash-completion ] \
	|| [ -f /usr/share/bash-completion/pyplayer ] \
	|| echo ln -s $(SOURCE_DIR)/pyplayer_bashcomp.sh /usr/share/bash-completion/pyplayer
	@echo -e "\n##########################"
	@echo -e "# install completed/verified successfully #"

install: $(LIB_INSTALL_FILES)
	@[ $(shell whoami) != root ] || make install_root
	@[ $(shell whoami) == root ] || make install_user
	# The preceding output is the commands to be executed for performing the install
	# The following sbould perform the install as executing the commands above:
	# make install 2>/dev/null | sed "s/^make\[/\#\ &/" | sh

clean:
	@echo "# pipe to shell or copy-paste in a sufficiently-privileged shell to complete"
	@echo "# ============================"
	@echo rm -f $(CONF_FILE_GLOBAL) $(RUN_DIR)/pyplayerd.pid $(LIB_INSTALL_FILES)
	@echo rmdir $(CONF_DIR) $(RUN_DIR) $(INSTALL_DIR) $(LIB_INSTALL_DIR)
	@echo "# ============================"

