#!/bin/sh
# Bryant Hansen

ME="$(basename "$0")"

PYPLAYERD=/usr/local/bin/pyplayerd.py
RUN_FILE=/run/pyplayer/pyplayerd.pid
LOG_FILE=/var/log/pyplayer/pyplayerd.log
PYPLAYERD_USER=pyplayerd
PYPLAYERD_GROUP=pyplayerd
PROJECT_DIR=/opt/critter_list

CONF_FILE="/etc/pyplayer/pyplayerd.conf"
[[ -f "$CONF_FILE" ]] && . "$CONF_FILE"
CONF_FILE="${HOME}/pyplayer/pyplayerd.conf"
[[ -f "$CONF_FILE" ]] && . "$CONF_FILE"
CONF_FILE="./pyplayerd.conf"
[[ -f "$CONF_FILE" ]] && . "$CONF_FILE"
CONF_FILE="/etc/pyplayer/pyplayerd.conf"

# TODO: add auto-generation comments to conf file
config="
            RUN_FILE=$RUN_FILE
            LOG_FILE=$LOG_FILE
            PYPLAYERD_USER=$PYPLAYERD_USER
            PYPLAYERD_GROUP=$PYPLAYERD_GROUP
            PYPLAYERD=$PYPLAYERD
        "

commands="

    if [[ ! -d '"$(dirname "$PROJECT_DIR")"' ]] ; then
        echo \"${ME}: ERROR: PROJECT_DIR parent dir '$(dirname "$PROJECT_DIR")' not found.\"
    fi

    if [[ ! -d '$PROJECT_DIR' ]] ; then
        echo \"${ME}: ERROR: PROJECT_DIR '$PROJECT_DIR' not found.\"
    fi

    if ! id -u $PYPLAYERD_USER > /dev/null ; then
        echo \"${ME}: ERROR: PYPLAYERD_USER '$PYPLAYERD_USER' does not exist\"
    fi

    if ! id -g $PYPLAYERD_GROUP > /dev/null ; then
        echo \"${ME}: ERROR: PYPLAYERD_GROUP '$PYPLAYERD_GROUP' does not exist\"
    fi

    if [[ ! -d '$(dirname "$CONF_FILE")' ]] ; then
        echo \"${ME}: ERROR: CONF_FILE directotry '$(dirname "$CONF_FILE")' does not exist\"
    fi

    if [[ ! -f '$CONF_FILE' ]] ; then
        echo \"${ME}: ERROR: CONF_FILE '$CONF_FILE' does not exist\"
    else
        if [[ \"\$(stat --format="%U" '$CONF_FILE')\" != ${PYPLAYERD_USER} ]] ; then
            echo \"${ME}: ERROR: CONF_FILE '$CONF_FILE' has incorrect user ownership: \$(stat --format="%U" '$CONF_FILE')\"
        fi
        if [[ \"\$(stat --format="%G" '$CONF_FILE')\" != ${PYPLAYERD_GROUP} ]] ; then
            echo \"${ME}: ERROR: CONF_FILE '$CONF_FILE' has incorrect group ownership: \$(stat --format="%G" '$CONF_FILE')\"
        fi
    fi

    if [[ ! -d '$(dirname "$RUN_FILE")' ]] ; then
        echo \"${ME}: ERROR: RUN_DIR '$(dirname "$RUN_FILE")' does not exist\"
    else
        if [[ \"\$(stat --format="%U" '$(dirname "$RUN_FILE")')\" != ${PYPLAYERD_USER} ]] ; then
            echo \"${ME}: ERROR: RUN_DIR '$(dirname "$RUN_FILE")' has incorrect user ownership: \$(stat --format="%U" '$(dirname "$RUN_FILE")')\"
        fi
        if [[ \"\$(stat --format="%G" '$(dirname "$RUN_FILE")')\" != ${PYPLAYERD_USER} ]] ; then
            echo \"${ME}: ERROR: RUN_DIR '$(dirname "$RUN_FILE")' has incorrect group ownership: \$(stat --format="%G" '$(dirname "$RUN_FILE")')\"
        fi
    fi

    if [[ ! -d '$(dirname "$LOG_FILE")' ]] ; then
        echo \"${ME}: ERROR: LOG_DIR '$(dirname "$LOG_FILE")' does not exist\"
    else
        if [[ \"\$(stat --format="%U" '$(dirname "$LOG_FILE")')\" != ${PYPLAYERD_USER} ]] ; then
            echo \"${ME}: ERROR: LOG_DIR '$(dirname "$LOG_FILE")' has incorrect user ownership: \$(stat --format="%U" '$LOG_DIR')\"
        fi
        if [[ \"\$(stat --format="%G" '$(dirname "$LOG_FILE")')\" != ${PYPLAYERD_USER} ]] ; then
            echo \"${ME}: ERROR: LOG_DIR '$(dirname "$LOG_FILE")' has incorrect group ownership: \$(stat --format="%G" '$LOG_DIR')\"
        fi
    fi

    status=\"\$($PYPLAYERD status)\"
    echo -e \"\$status\"
"
echo "######################################################"
echo "# To install, simply execute the following commands as root or sudo:"
echo "######################################################"
echo -e "$commands"
echo "######################################################"
echo ""
echo "# Tip: root can pipe the commands to the shell, like so: "
echo "#   $0 | ${SHELL}"
