#!/bin/sh
# Bryant Hansen

# initial 1-liner:
# pyplayerd.py stop ; pyplayerd.py start ; pyplayer add_url_to_queue /data/media/mp3/Rammstein/Liebe_Ist_Für_Alle_Da/04\ -\ Haifisch.mp3 ; pyplayer play_next_url ; sleep 5 ; pyplayer get_title

#pyplayer get_all_information

pyplayer get_album
pyplayer get_artist
pyplayer get_audio_bitrate
pyplayer get_audio_samples
pyplayer get_current_time
pyplayer get_file_name
pyplayer get_meta_album
pyplayer get_meta_artist
pyplayer get_meta_comment
pyplayer get_meta_genre
pyplayer get_meta_title
pyplayer get_meta_track
pyplayer get_meta_year
pyplayer get_percent_pos
pyplayer get_state
pyplayer get_sub_visibility
pyplayer get_time_length
pyplayer get_time_pos
pyplayer get_title
pyplayer get_track
pyplayer get_track_length
pyplayer get_url
pyplayer get_video_bitrate
pyplayer get_video_codec
pyplayer get_video_resolution
pyplayer get_vo_fullscreen
pyplayer get_volume1
pyplayer get_volume2
