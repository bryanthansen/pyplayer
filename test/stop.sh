#!/bin/sh
# Bryant Hansen

ps u | head -n 1
ps auwx | grep "py\|mplay" | grep -v "grep\|tail"
./pyplayerd.py stop
ps auwx | grep "py\|mplay" | grep -v "grep\|tail"

if [[ -f /run/pyplayer/mplayer.pid  ]] ; then
    echo "$0: /run/pyplayer/mplayer.pid should not still exist." >&2
fi
