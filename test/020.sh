#!/bin/sh
# Bryant Hansen

# initial 1-liner:
# pyplayerd.py stop ; pyplayerd.py start ; pyplayer add_url_to_queue /data/media/mp3/Rammstein/Liebe_Ist_Für_Alle_Da/04\ -\ Haifisch.mp3 ; pyplayer play_next_url ; sleep 5 ; pyplayer get_title

file="/data/media/mp3/Rammstein/Liebe_Ist_Für_Alle_Da/04\ -\ Haifisch.mp3"
[[ "$1" ]] && file="$1"

pyplayerd.py stop
pyplayerd.py start
pyplayer add_url_to_queue "$file"
sleep 2
pyplayer play
sleep 5
title="$(pyplayer get_title)"

FAIL=""
if [[ "$title" == "Haifisch" ]] ; then
    echo "TEST PASSED"
else
    echo "TEST FAILED"
    FAIL=1
fi

track="$(pyplayer get_track)"
echo "track=$track"

#percent_pos1="$(pyplayer get_percent_pos)"
#sleep 5
#percent_pos1="$(pyplayer get_percent_pos)"
