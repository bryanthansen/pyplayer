# from:
#   http://tldp.org/LDP/abs/html/tabexpansion.html

# file: UseGetOpt-2
# UseGetOpt-2.sh parameter-completion

# TODO: parse pyplayer.py help menu to get this
commands="
   add_url_to_queue
   cleanup
   do_command
   dump_output
   dump_queue
   flush_output
   flush_queue
   get_album
   get_all_information
   get_artist
   get_current_time
   get_genre
   get_next_in_queue
   get_percent_pos
   get_state
   get_title
   get_track
   get_track_length
   get_volume1
   get_volume2
   get_year
   help
   launch_player
   mplayer_thread
   pause
   perform_command
   play_next_url
   playurl
   queue_url_next
   quit
   replace_queue_with_url
   seek
   seek_end
   seek_start
   set_current_pos
   stop
"

help_commands="$(pyplayer help 2>&1 | sed -n -e '/Functions\ supported/,/========/p' | sed '/Functions\ supported/d;/==========/d')"
if [[ "$help_commands" ]] ; then
    commands="$help_commands"
fi

# from a post in stackexchange:
#   http://unix.stackexchange.com/questions/77009/custom-autocomplete-deal-with-spaces-in-filenames
_zaso()
{
    local dir="$1"

    pushd "$dir" >/dev/null
    find * -maxdepth 0 2>/dev/null
    popd >/dev/null
}

_pyplayer ()   #  By convention, the function name
{                 #+ starts with an underscore.
  #local cur
  local cmd cur prev
  # Pointer to current completion word.
  # By convention, it's named "cur" but this isn't strictly necessary.

  COMPREPLY=()   # Array variable storing the possible completions.
  cur=${COMP_WORDS[COMP_CWORD]}
  cmd=${COMP_WORDS[0]}
  prev="${COMP_WORDS[COMP_CWORD-1]}"


  #case "$cur" in
    # -*)
    #COMPREPLY=( $( compgen -W '-a -d -f -l -t -h --aoption --debug \
    #                           --file --log --test --help --' -- $cur ) );;
    case $prev in

    add_url_to_queue)
        #printf "prev=$prev args='$*'\n"
        #printf "PWD=$PWD \n" >&2
        _filedir
    ;;

    playurl| \
    replace_queue_with_url)
        #_fileDir
            # -F _cd -o nospace

        list_files_with_find="1"
        get_dir_with__filedir=""

        if [[ "$list_files_with_find" ]] ; then
            local cur dir
            local IFS=$'\n'

            cur="$1"
            dir="$2"

            compopt -o filenames 2>/dev/null
            files="$(
                pushd "$dir" >/dev/null
                find * -maxdepth 0 2>/dev/null
                popd >/dev/null
            )"
            COMPREPLY=( $( compgen -W "$files" -- "$cur" ) );
            return 0
        elif [[ "$get_dir_with__filedir" ]] ; then
            # TODO: auto-detect the bashcomp dir
            pushd "/usr/share/bash-completion/" >/dev/null
            _filedir
            popd >/dev/null
            return 0
        else
            COMPREPLY=( $( compgen -f -o nospace -- $cur ) )
            return 0
        fi
    ;;
    $cmd)
        COMPREPLY=( $( compgen -W ' $commands ' -- $cur ) )
    ;;
    esac

#   Generate the completion matches and load them into $COMPREPLY array.
#   xx) May add more cases here.
#   yy)
#   zz)
  #esac

  return 0
}

complete -F _pyplayer -o filenames ./pyplayer.py pyplayer.py pyplayer
