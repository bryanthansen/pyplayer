Front end for mplayer - exposes entire slave API, both commands & responses

Communicates via a socket using RPC calls

Can connect via localhost or remote - enabling a media server on a separate, shared device

Dynamic Queue - can be flushed and reloaded while current track is playing

Callback on track-complete
